﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Serialization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MFC {
  public class Level {
    List<Note> notes = new List<Note>();
    List<Note> eNotes = new List<Note>();
    string song;
    double bpm;
    int combo = 0;
    double lastScore = 0;
    Texture2D lastJudge;
    int score = 0;
    Texture2D background;
    Texture2D pixel;
    List<Texture2D> noteTextures;
    List<Texture2D> comboTextures;
    List<Texture2D> scoreTextures;
    dynamic level;
    string levelPath;
    public Level(Texture2D pixelTex, string path, GraphicsDevice graphicsDevice, List<Texture2D> bruh1, List<Texture2D> bruh2, List<Texture2D> bruh3) {
      song = path + "song.ogg";
      FileStream bg = File.Open(Path.Combine(path + "bg.png"), FileMode.Open);
      pixel = pixelTex;
      lastJudge = pixel;
      background = Texture2D.FromStream(graphicsDevice, bg);
      bg.Close();
      noteTextures = bruh1;
      comboTextures = bruh2;
      scoreTextures = bruh3;
      Console.WriteLine(scoreTextures.Count());
      Console.WriteLine(bruh3.Count());
      level = JsonConvert.DeserializeObject(File.ReadAllText(path + "info.json"));
      levelPath = path + "ez.mfc";
      bpm = level["bpm"];
      ReloadLevel();
    }

    public void Start(GameAudio gameAudio) {
      ReloadLevel();
      gameAudio.PlaySong(song);
    }

    public void Update(double songBeat) {
      List<Note> notesToRemove = new List<Note>();
      foreach (Note note in notes) {
        double score = note.GetScore(songBeat);
        if (score < -0.5) {
          combo = 0;
          lastScore = score;
          lastJudge = scoreTextures[4];
          notesToRemove.Add(note);
        }
      }
      foreach (Note toRemove in notesToRemove) {
        notes.Remove(toRemove);
      }
    }

    public void Draw(SpriteBatch spriteBatch, double xMod, GameAudio gameAudio, bool isEdit) {
      foreach (Note note in isEdit ? eNotes : notes) {
        note.Draw(spriteBatch, gameAudio.GetSongBeat((long)bpm), xMod);
      }

    }

    public void HitNote(Keys key, bool downup, double songBeat) {
      if (downup == false) {
        // down
        switch (key) {
          case Keys.A:
            HitRow(1, songBeat);
            break;
          case Keys.S:
            HitRow(2, songBeat);
            break;
          case Keys.D:
            HitRow(3, songBeat);
            break;
          case Keys.F:
            HitRow(4, songBeat);
            break;
          case Keys.J:
            HitRow(5, songBeat);
            break;
          case Keys.K:
            HitRow(6, songBeat);
            break;
          case Keys.L:
            HitRow(7, songBeat);
            break;
          case Keys.None: //cope
            HitRow(8, songBeat);
            break;
          case Keys.Space:
            HitRow(0, songBeat);
            break;
        }
      } else {
        //up, potentially release a hold
        switch (key) {
          case Keys.A:
            HitHoldRow(1, songBeat);
            break;
          case Keys.S:
            HitHoldRow(2, songBeat);
            break;
          case Keys.D:
            HitHoldRow(3, songBeat);
            break;
          case Keys.F:
            HitHoldRow(4, songBeat);
            break;
          case Keys.J:
            HitHoldRow(5, songBeat);
            break;
          case Keys.K:
            HitHoldRow(6, songBeat);
            break;
          case Keys.L:
            HitHoldRow(7, songBeat);
            break;
          case Keys.None:
            HitHoldRow(8, songBeat);
            break;
          case Keys.Space:
            HitHoldRow(0, songBeat);
            break;
        }
      }
    }

    public int GetLeft(bool isEdit) {
      return isEdit ? eNotes.Count() : notes.Count();
    }

    void HitRow(int row, double songBeat) {
      foreach (Note note in notes) {
        if (note.GetRow() == row && note.IsHeld() == false) {
          double score = note.GetScore(songBeat);
          if (score > 0.5 || score < -0.5) {
            if (score > 0.65 || score < -0.65) {
              break;
            }
            lastJudge = scoreTextures[4];
            combo = 0;
          } else {
            combo++;
            double absScore = Math.Abs(score);
            if (absScore < 0.08) {
              lastJudge = scoreTextures[0];
            } else if (absScore < 0.16) {
              lastJudge = scoreTextures[1];
            } else if (absScore < 0.3) {
              lastJudge = scoreTextures[2];
            } else if (absScore < 0.5) {
              lastJudge = scoreTextures[3];
            }
            if (note.IsHold()) {
              note.Held();
            } else {
              notes.Remove(note);
            }
          }
          lastScore = score;
          break;
        }
      }
    }

    void HitHoldRow(int row, double songBeat) {
      foreach (Note note in notes) {
        if (note.GetRow() == row && note.IsHeld() == true) {
          double score = note.GetScore(songBeat);
          if (score > 0.5 || score < -0.5) {
            lastJudge = scoreTextures[4];
            combo = 0;
          } else {
            combo++;
            double absScore = Math.Abs(score);
            if (absScore < 0.08) {
              lastJudge = scoreTextures[0];
            } else if (absScore < 0.16) {
              lastJudge = scoreTextures[1];
            } else if (absScore < 0.3) {
              lastJudge = scoreTextures[2];
            } else if (absScore < 0.5) {
              lastJudge = scoreTextures[3];
            }
            notes.Remove(note);
          }
          lastScore = score;
          break;
        }
      }
    }

    public void AddNote(Keys key, int editKey, double songBeat) {
      int rowKeyIDontKnowOk;
      switch (key) {
        case Keys.Q:
          rowKeyIDontKnowOk = 1;
          break;
        case Keys.W:
          rowKeyIDontKnowOk = 2;
          break;
        case Keys.E:
          rowKeyIDontKnowOk = 3;
          break;
        case Keys.R:
          rowKeyIDontKnowOk = 4;
          break;
        case Keys.T:
          rowKeyIDontKnowOk = 5;
          break;
        case Keys.Y:
          rowKeyIDontKnowOk = 6;
          break;
        case Keys.U:
          rowKeyIDontKnowOk = 7;
          break;
        case Keys.I:
          rowKeyIDontKnowOk = 8;
          break;
        default:
          rowKeyIDontKnowOk = 0;
          break;
      }
      bool add = false;
      foreach (Note note in eNotes) {
        if (note.IsSame(rowKeyIDontKnowOk, editKey, songBeat)) {
          add = false;
          eNotes.Remove(note); //holds are dealt with, i think?
          break;
        } else {
          add = true;
        }
      }
      if (add) {
        eNotes.Add(new Note(pixel, noteTextures[editKey-1], songBeat, editKey, rowKeyIDontKnowOk, 0));
      }
    }

    public void SyncNotes() {
      List<Note> unsortedNotes = new List<Note>();
      foreach (Note note in eNotes) {
        unsortedNotes.Add(note);
      }
      notes = unsortedNotes.OrderBy(o => o.beat).ToList();
    }

    public bool SaveLevel() {
      SyncNotes();
      List<String> levelDotTxt = new List<String>();
      foreach (Note note in notes) {
        levelDotTxt.Add(note.beat + " " + note.type + " " + note.row + " " + note.length);
      }
      try {
        System.IO.File.WriteAllLines(levelPath, levelDotTxt);
        return true;
      } catch {
        return false;
      }
    }

    public void ReloadLevel() {
      StreamReader file = new StreamReader(levelPath);
      string line;
      notes = new List<Note>();
      eNotes = new List<Note>();
      while ((line = file.ReadLine()) != null) {
        notes.Add(new Note(pixel, noteTextures[int.Parse(line.Split(' ')[1])-1], double.Parse(line.Split(' ')[0]), int.Parse(line.Split(' ')[1]), int.Parse(line.Split(' ')[2]), double.Parse(line.Split(' ')[3])));
        eNotes.Add(new Note(pixel, noteTextures[int.Parse(line.Split(' ')[1]) - 1], double.Parse(line.Split(' ')[0]), int.Parse(line.Split(' ')[1]), int.Parse(line.Split(' ')[2]), double.Parse(line.Split(' ')[3])));
        //todo: fix sphageti.
      }
      file.Close();
      SyncNotes();
    }

    public double GetBpm() {
      return bpm;
    }

    public long GetBpmLong() {
      return (long)bpm;
    }

    public int GetCombo() //todo: move these to run inside of the level function instead ? maybe? not sure...
    {
      return combo;
    }

    public double GetScore() {
      return lastScore;
    }

    public Texture2D GetJudge() {
      return lastJudge;
    }

    public Texture2D GetBackground() {
      return background;
    }

    public dynamic GetLevelInfo() {
      return level;
    }
  }
}
