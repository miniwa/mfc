﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManagedBass;

namespace MFC {
  public class GameAudio {
    int song = 0;
    double offset = 0;
    public GameAudio() {
      //initialize audio
      if (Bass.Init()) {
        Console.WriteLine("Bass initialized");
      } else {
        Console.WriteLine("Error: {0}!", Bass.LastError);
        System.Environment.Exit(1);
      }
    }

    public void SetOffset(double userOffset, double songOffset) {
      offset = userOffset + songOffset;
    }

    public bool PlaySong(string location) {
      if (song != 0) {
        Bass.ChannelStop(song);
      }

      song = Bass.CreateStream(location);

      if (song != 0) {
        Bass.ChannelPlay(song); // Play the stream
        return true;
      } else {
        Console.WriteLine("PlaySong error: {0}", Bass.LastError);
        return false;
      }
    }

    public bool StopSong() {
      if (Bass.ChannelStop(song)) {
        return true;
      } else {
        Console.WriteLine("StopSong error: {0}", Bass.LastError);
        return false;
      }
    }

    public bool PauseSong() {
      if (Bass.ChannelPause(song)) {
        return true;
      } else {
        Console.WriteLine("PauseSong error: {0}", Bass.LastError);
        return false;
      }
    }

    public bool UnPauseSong() {
      if (Bass.ChannelPlay(song)) {
        return true;
      } else {
        Console.WriteLine("UnPauseSong error: {0}", Bass.LastError);
        return false;
      }
    }

    public double GetSongTime() {
      return Bass.ChannelBytes2Seconds(song, Bass.ChannelGetPosition(song) - long.Parse(offset.ToString()));
    }

    public double GetSongBeat(long bpm) {
      return Bass.ChannelBytes2Seconds(song, Bass.ChannelGetPosition(song) - long.Parse(offset.ToString())) * bpm / 60;
    }

    public double GetRoundedBeat(long bpm) {
      return Bass.ChannelSeconds2Bytes(song, Math.Floor(GetSongBeat(bpm)) / bpm * 60);
    }

    public bool SetSongBeat(double beat) {
      if (Bass.ChannelSetPosition(song, (long)beat)) {
        return true;
      } else {
        Console.WriteLine("SetSongBeat error: {0}", Bass.LastError);
        return false;
      }
    }

    public bool ChangeSongBeat(double beat, double bpm) {
      if (Bass.ChannelSetPosition(song, Bass.ChannelGetPosition(song) + Bass.ChannelSeconds2Bytes(song, (beat / bpm * 60)))) {
        return true;
      } else {
        Console.WriteLine("ChangeSongBeat error: {0}", Bass.LastError);
        return false;
      }
    }

    public double GetSongBeats(long bpm) {
      return Bass.ChannelBytes2Seconds(song, Bass.ChannelGetLength(song)) * bpm / 60;
    }

    public double GetSongPercent() {
      return Bass.ChannelBytes2Seconds(song, Bass.ChannelGetPosition(song) - long.Parse(offset.ToString())) / Bass.ChannelBytes2Seconds(song, Bass.ChannelGetLength(song));
    }

    public PlaybackState SongStatus() {
      return Bass.ChannelIsActive(song);
    }

    public bool SongPlaying() {
      if (Bass.ChannelIsActive(song) == PlaybackState.Playing) {
        return true;
      } else {
        return false; //this includes stalled, which might gives us issues in the future. consider changing to a switch?
                      //...or just assume peoples pc's are good enough to play fucking music lmao
      }
    }
  }
}
