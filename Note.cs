﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MFC {
  public class Note {
    Texture2D texture;
    Texture2D pixel;
    public double beat;
    public int type;
    public int row;
    double endBeat;
    public double length;
    bool held = false;
    public Note(Texture2D yoPixel, Texture2D noteTexture, double noteBeat, int noteType, int noteRow, double holdLength) {
      //initialize the faken note
      texture = noteTexture;
      pixel = yoPixel;
      beat = noteBeat;
      type = noteType;
      row = noteRow;
      length = holdLength;
      endBeat = (holdLength != 0) ? noteBeat + holdLength : 0;
    }

    public void Draw(SpriteBatch spriteBatch, double songBeat, double xMod) {
      if (endBeat != 0) {
        spriteBatch.Draw(texture, // the hold itself
            new Rectangle(445 + (GetWidth(type) * (row - 1) + GetPadding(type, row) * (row - 1)), //x
            653 - ((int)(endBeat * xMod) - (int)(songBeat * xMod)), //y
            GetWidth(type), //width
            (int)Math.Floor(length * xMod) - 16), //height
            Color.White); //color
        spriteBatch.Draw(pixel, // beginning note thing
            new Rectangle(445 + (GetWidth(type) * (row - 1) + GetPadding(type, row) * (row - 1)),
            653 - 16 - ((int)(beat * xMod) - (int)(songBeat * xMod)),
            GetWidth(type),
            16),
            GetColorHold(type));
        spriteBatch.Draw(pixel, // end note thing
            new Rectangle(445 + (GetWidth(type) * (row - 1) + GetPadding(type, row) * (row - 1)),
            653 - 16 - ((int)(endBeat * xMod) - (int)(songBeat * xMod)),
            GetWidth(type),
            16),
            GetColorHold(type));
      } else {
        spriteBatch.Draw(texture,
            new Rectangle(445 + (GetWidth(type) * (row - 1) + GetPadding(type, row) * (row - 1)),
            653 - 16 - ((int)(beat * xMod) - (int)(songBeat * xMod)),
            GetWidth(type),
            16),
            Color.White);
      }
    }

    int GetWidth(int type) {
      switch (type) { //todo: consider changing this to be not zero-indexed
        case 1:
          return 390; //6
        case 2:
          return 184; //6
        case 3:
          return 122; //6
        case 4:
          return 90; //6, more or less
        case 5:
          return 70; //6
        case 6:
          return 56; //6
        case 7:
          return 46; //6, more or less
        case 8:
          return 38; //6, more or less
        default:
          return 69;
      }
    }

    int GetPadding(int type, int row) {
      switch (type) {
        case 1:
          return 6;
        case 2:
          return 22;
        case 3:
          return 10;
        case 4:
          return 10;
        case 5:
          return 9;
        case 6:
          return 10;
        case 7:
          return 11;
        case 8:
          return 12;
        default:
          return 6;
      }
    }

    Color GetColorHold(int type) {
      switch (type) {
        case 1:
          return new Color(0xFFE7FFA9);
        case 2:
          return new Color(0xFFC4FBF8);
        case 3:
          return new Color(0xFFFEC9DD);
        case 4:
          return new Color(0xFFFFF5BA);
        case 5:
          return new Color(0xFFF6A6FF);
        case 6:
          return new Color(0xFFB6B9FF);
        case 7:
          return new Color(0xFFADF8D7);
        case 8:
          return new Color(0xFFFFCBC0);
        default:
          return Color.Gray;
      }
    }

    public bool IsSame(int keyRow, int keyType, double keyBeat) //todo: clean this fucking method up when you dont need the debug messages ever again
    {
      if (keyRow == row) {
        //Console.WriteLine("row matches");
      } else {
        return false;
      }
      if (keyType == type) {
        //Console.WriteLine("type matches");
      } else {
        return false;
      }
      if (keyBeat == beat) {
        //Console.WriteLine("keybeat matches");
      } else {
        return false;
      }
      return true;
    }

    public int GetRow() {
      switch (type) {
        case 1:
          return 0;
        case 2:
          return row + 3;
        case 3:
          if (row == 2) {
            return 0;
          } else {
            if (row > 2) {
              return row + 2;
            } else {
              return row + 3;
            }

          }
        case 4:
          return row + 2;
        case 5:
          if (row == 3) {
            return 0;
          } else {
            if (row > 3) {
              return row + 1;
            } else {
              return row + 2;
            }
          }
        case 6:
          return row + 1;
        case 7:
          if (row == 4) {
            return 0;
          } else {
            if (row > 4) {
              return row;
            } else {
              return row + 1;
            }
          }
        case 8:
          return row;
        default:
          return 0; //should be dead code, lol
      }
    }

    public double GetScore(double songBeat) {
      if (held) {
        return endBeat - songBeat;
      } else {
        return beat - songBeat;
      }

    }

    public void Held() {
      held = true;
    }

    public void UnHeld() {
      held = false; //dont think this function will ever be used ?
    }

    public bool IsHeld() {
      return held;
    }

    public bool IsHold() {
      return endBeat > 0 ? true : false;
    }
  }
}
