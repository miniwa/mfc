﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Diagnostics;
using System.Collections.ObjectModel;
using MonoGame.Extended.Input;
using MonoGame.Extended.Input.InputListeners;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework.Media;
using MonoGame.Extended.Sprites;

namespace MFC {
  /// <summary>
  /// This is the main type for your game.
  /// </summary>
  public class Game1 : Game {
    GraphicsDeviceManager graphics;
    SpriteBatch spriteBatch;
    GameAudio gameAudio;
    SpriteFont fontSmall;
    SpriteFont fontMedium;
    Texture2D gear;
    Texture2D pixel;
    Texture2D selarrow;
    Level level;
    Random rng;
    string dataDir;
    double xMod = 300;
    int editModifier = 4;
    int editKey = 4;
    int selection = 0;
    List<String> songs = new List<string>();
    enum MFCState {
      Menu,
      Game,
      Edit
    }
    MFCState gameState = MFCState.Menu;
    public Game1() {
      graphics = new GraphicsDeviceManager(this);
      graphics.PreferredBackBufferHeight = 720;
      graphics.PreferredBackBufferWidth = 1280;
      Content.RootDirectory = "Content";
    }

    protected override void Initialize() {
      dataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/MFC/";
      KeyboardListenerSettings ugh = new KeyboardListenerSettings();
      ugh.RepeatPress = false;
      ugh.InitialDelayMilliseconds = 0;
      ugh.RepeatDelayMilliseconds = 0;
      KeyboardListener keyboardListener = new KeyboardListener(ugh);
      Components.Add(new InputListenerComponent(this, keyboardListener));
      keyboardListener.KeyPressed += (sender, args) => { HandleInput(sender, args, false); };
      keyboardListener.KeyReleased += (sender, args) => { HandleInput(sender, args, true); };
      gameAudio = new GameAudio();
      //Window.IsBorderless = true
      rng = new Random();
      foreach (string folder in Directory.GetDirectories(dataDir + "/songs/")) {
        songs.Add(folder);
      }
      base.Initialize();
    }

    protected override void LoadContent() {
      // Create a new SpriteBatch, which can be used to draw textures.
      spriteBatch = new SpriteBatch(GraphicsDevice);
      fontSmall = Content.Load<SpriteFont>("FontSmall");
      fontMedium = Content.Load<SpriteFont>("FontMedium");
      gear = Content.Load<Texture2D>("Gear");
      pixel = Content.Load<Texture2D>("Pixel");
      selarrow = Content.Load<Texture2D>("SelectArrow");
      // TODO: use this.Content to load your game content here
    }

    protected override void UnloadContent() {
      // TODO: Unload any non ContentManager content here
    }

    protected override void Update(GameTime gameTime) //todo: implement a setState() similar to USG because holy heck i need it dont i lol
    {
      switch (gameState) {
        case MFCState.Menu:
          break;
        case MFCState.Game:
          if (level != null) {
            level.Update(gameAudio.GetSongBeat(level.GetBpmLong()));
          }
          break;
        case MFCState.Edit:
          if (level != null) {
            //level.Update(gameAudio.GetSongBeat(level.GetBpmLong())); //note to self: maybe have to modify this function later?
          }
          break;
      }
      base.Update(gameTime);
    }

    protected override void Draw(GameTime gameTime) {
      GraphicsDevice.Clear(Color.Black); //HEY FOR ANYONE READING ILL PUT THIS SHIT INTO CLASSES OK DONT WORRY I KNOW WHAT OBJECT ORIENTED PROGRAMMING IS

      spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied);

      switch (gameState) {
        case MFCState.Menu:
          int c = 48;
          spriteBatch.DrawString(fontMedium, "Select a song with the arrow keys and then press enter to play it nanodesu~", new Vector2(0, 0), Color.White);
          spriteBatch.Draw(selarrow, new Vector2(4, selection * 24 + 48), Color.White);
          foreach (string folder in songs) {
            spriteBatch.DrawString(fontMedium, folder, new Vector2(40, c), Color.White);
            c += 24;
          }
          break;
        case MFCState.Game:
          if (level != null) {
            spriteBatch.Draw(level.GetBackground(), new Vector2(0), Color.White);
            spriteBatch.Draw(gear, new Vector2(430, 0), Color.White);
            level.Draw(spriteBatch, xMod, gameAudio, false);
            spriteBatch.Draw(level.GetJudge(), new Vector2(440, 400), Color.White);
            spriteBatch.DrawString(fontMedium, level.GetCombo().ToString(), new Vector2((1280 / 2) - 8, 400), Color.White);
            spriteBatch.DrawString(fontSmall, level.GetScore().ToString(), new Vector2((1280 / 2) - 6, 420), Color.White);
            spriteBatch.DrawString(fontSmall, level.GetLeft(false).ToString(), new Vector2(0, 500), Color.White);
          } else {
            spriteBatch.DrawString(fontMedium, "level loading", new Vector2(0, 0), Color.White);
          }
          if (!gameAudio.SongPlaying()) {
            spriteBatch.DrawString(fontMedium, "press escape to restart song, or backspace in edit mode to play song", new Vector2(0, 0), Color.White);
            spriteBatch.DrawString(fontMedium, "press tab to enter edit mode, by the way", new Vector2(0, 24), Color.White);
          }
          break;
        case MFCState.Edit:
          if (level != null) {
            spriteBatch.Draw(gear, new Vector2(430, 0), Color.White);
            level.Draw(spriteBatch, xMod, gameAudio, true);
          }
          spriteBatch.DrawString(fontMedium, gameAudio.GetSongTime().ToString() + " TIME", new Vector2(0, 0), Color.White);
          if (level != null) {
            spriteBatch.DrawString(fontMedium, gameAudio.GetSongBeat(level.GetBpmLong()).ToString() + " BEAT", new Vector2(0, 24), Color.White);
            spriteBatch.DrawString(fontSmall, level.GetLeft(true).ToString(), new Vector2(0, 500), Color.White);
          } else {
            spriteBatch.DrawString(fontMedium, "0 BEAT", new Vector2(0, 24), Color.White);
          }
          spriteBatch.DrawString(fontMedium, Math.Round(gameAudio.GetSongPercent() * 100).ToString() + "%", new Vector2(0, 48), Color.White);
          spriteBatch.DrawString(fontMedium, "Playing: " + gameAudio.SongPlaying().ToString(), new Vector2(0, 48 + 24), Color.White);
          spriteBatch.DrawString(fontMedium, "EMOD: " + editModifier.ToString(), new Vector2(0, 48 + 48), Color.White);
          spriteBatch.DrawString(fontMedium, "EKEY: " + editKey.ToString(), new Vector2(0, 48 + 48 + 24), Color.White);
          spriteBatch.DrawString(fontSmall, "Controls: Tab = exit; Backspace = play/pause; QWERTYUI = place/delete notes; 12345678 = select note type;", new Vector2(0, 720 - 52), Color.White);
          spriteBatch.DrawString(fontSmall, "Up/Down arrows to go up and down in editor; Shift+Up/Down to change EMOD (note placement or something); Shift+S saves", new Vector2(0, 720 - 36), Color.White);
          spriteBatch.DrawString(fontSmall, "In play mode, use ASDF(space)JKL; to hit notes; ESC to restart song; O and P to change speed mod", new Vector2(0, 720 - 20), Color.White);
          break;
      }

      spriteBatch.End();
      base.Draw(gameTime);
    }

    void HandleInput(object sender, KeyboardEventArgs args, bool downup) //possible todo: move these into their own classes where they handle input themselves?
    {                                                                    //if anything, it'll make the code a lot cleaner.
      switch (gameState) {
        case MFCState.Menu:
          if (args.Key == Keys.Down && downup == false) {
            selection = Math.Clamp(selection + 1, 0, songs.Count - 1);
          }
          if (args.Key == Keys.Up && downup == false) {
            selection = Math.Clamp(selection - 1, 0, songs.Count - 1);
          }
          if (args.Key == Keys.Enter && downup == false) {
            List<Texture2D> noteTextures = new List<Texture2D>();
            for (int i = 1; i < 9; i++) {
              noteTextures.Add(Content.Load<Texture2D>("n" + i.ToString()));
            }
            List<Texture2D> comboTextures = new List<Texture2D>();
            for (int i = 0; i < 10; i++) {
              comboTextures.Add(Content.Load<Texture2D>("c" + i.ToString()));
            }
            List<Texture2D> scoreTextures = new List<Texture2D>();
            scoreTextures.Add(Content.Load<Texture2D>("perfectplus"));
            scoreTextures.Add(Content.Load<Texture2D>("perfect"));
            scoreTextures.Add(Content.Load<Texture2D>("good"));
            scoreTextures.Add(Content.Load<Texture2D>("hit"));
            scoreTextures.Add(Content.Load<Texture2D>("miss"));
            level = new Level(pixel, songs[selection] + "/", GraphicsDevice, noteTextures, comboTextures, scoreTextures);
            gameState = MFCState.Game;
          }
          break;
        case MFCState.Game:
          if (args.Key == Keys.Escape && downup == false) {
            level.Start(gameAudio);
          }
          if (args.Key == Keys.Tab && downup == false) {
            gameState = MFCState.Edit;
          }
          if (level != null && (args.Key == Keys.A || args.Key == Keys.S || args.Key == Keys.D || args.Key == Keys.F || args.Key == Keys.J || args.Key == Keys.K || args.Key == Keys.L || args.Key == Keys.None || args.Key == Keys.Space)) {
            level.HitNote(args.Key, downup, gameAudio.GetSongBeat(level.GetBpmLong()));
          }
          if (args.Key == Keys.O && downup == false) {
            xMod -= 50;
          }
          if (args.Key == Keys.P && downup == false) {
            xMod += 50;
          }
          break;
        case MFCState.Edit:
          if (level != null && downup == false && (args.Key == Keys.Q || args.Key == Keys.W || args.Key == Keys.E || args.Key == Keys.R || args.Key == Keys.T || args.Key == Keys.Y || args.Key == Keys.U || args.Key == Keys.I)) {
            level.AddNote(args.Key, editKey, gameAudio.GetSongBeat(level.GetBpmLong()));
          }
          if (downup == false) {
            if (args.Modifiers == KeyboardModifiers.Shift) {
              switch (args.Key) {
                case Keys.Up:
                  editModifier += 1;
                  break;
                case Keys.Down:
                  editModifier -= 1;
                  break;
                case Keys.S:
                  level.SaveLevel();
                  break;
                case Keys.Z:
                  Console.WriteLine("the ability to undo is coming .... maybe? someday? who knows, lol.");
                  break;
                case Keys.R:
                  level.ReloadLevel();
                  break;
              }
            } else {
              switch (args.Key) {
                case Keys.Up:
                  gameAudio.ChangeSongBeat(1d / editModifier, level.GetBpm());
                  break;
                case Keys.Down:
                  gameAudio.ChangeSongBeat(1d / editModifier * -1d, level.GetBpm());
                  break;
                case Keys.Tab:
                  level.SyncNotes();
                  gameState = MFCState.Game;
                  break;
                case Keys.Back:
                  if (gameAudio.SongPlaying()) {
                    gameAudio.PauseSong();
                    gameAudio.SetSongBeat(gameAudio.GetRoundedBeat((long)level.GetBpm()));
                  } else {
                    gameAudio.UnPauseSong();
                  }
                  break;
                case Keys.D1: // there _HAS_ to be a more effective way to do this.. right? oh well... for now, it'll have to do, i guess >-<.
                  editKey = 1;
                  break;
                case Keys.D2:
                  editKey = 2;
                  break;
                case Keys.D3:
                  editKey = 3;
                  break;
                case Keys.D4:
                  editKey = 4;
                  break;
                case Keys.D5:
                  editKey = 5;
                  break;
                case Keys.D6:
                  editKey = 6;
                  break;
                case Keys.D7:
                  editKey = 7;
                  break;
                case Keys.D8:
                  editKey = 8;
                  break;
              }
            }
          }
          break;
      }
    }
  }
}
